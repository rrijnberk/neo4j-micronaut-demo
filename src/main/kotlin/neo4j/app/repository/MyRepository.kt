package neo4j.app.repository

import io.micronaut.context.annotation.Primary
import neo4j.app.domain.MyNode
import org.neo4j.driver.Driver
import org.neo4j.driver.Record
import org.neo4j.driver.Transaction
import java.util.stream.Stream
import javax.inject.Inject
import javax.inject.Singleton

@Primary
@Singleton
class MyRepository  {
    @Inject
    var driver: Driver? = null

    fun create(myNode: MyNode) {
        driver!!.session().use { s ->
            val statement = "CREATE (myNode:MyNode { name: '${myNode.name}' })"
            s.writeTransaction { tx -> tx.run(statement) }
        }
    }

    fun find(): Stream<MyNode?> {
        driver!!.session().use { s ->
            val statement = "MATCH (myNode:MyNode) RETURN myNode"
            return s.readTransaction { tx: Transaction -> tx.run(statement)
                    .list<MyNode> { record: Record -> MyNode(record.get("myNode").asMap()) }.stream() }
        }
    }
}
