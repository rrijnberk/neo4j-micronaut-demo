package neo4j.app.domain

class MyNode {
    var name: String = ""

    constructor(data: Map<*, *>) {
        this.name = data["name"] as String
    }
}
