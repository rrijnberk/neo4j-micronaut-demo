package neo4j.app

import io.micronaut.runtime.Micronaut.*

fun main(args: Array<String>) {
	build()
	    .args(*args)
		.packages("neo4j.app")
		.start()
}

