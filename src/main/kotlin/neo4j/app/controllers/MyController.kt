package neo4j.app.controllers

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import neo4j.app.domain.MyNode
import neo4j.app.repository.MyRepository
import java.util.stream.Stream
import javax.inject.Inject

@Controller("/")
class MyController {
    @Inject
    var repo: MyRepository? = null

    @Post("/create")
    fun create(myNode: MyNode): Number {
        repo?.create(myNode)
        return 200;
    }

    @Get("/list")
    fun company(): Stream<MyNode?>? {
        return repo?.find()
    }

    @Get("/health")
    @Produces(MediaType.TEXT_PLAIN)
    fun index(): String {
        return "My Controller is online."
    }
}
